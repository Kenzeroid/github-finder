import React from 'react';

export const NotFound = () => {
  return (
    <div>
      <h1>Not Found</h1>
      <p>Page your looking at is not found</p>
    </div>
  );
};

export default NotFound;
